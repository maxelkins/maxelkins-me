# maxelkins.me

Code for https://maxelkins.me.

## Commands

| Command             | Action                                           |
| :------------------ | :----------------------------------------------- |
| `npm i`             | Installs dependencies                            |
| `npm run dev`       | Starts local dev server at `localhost:4321`      |
| `npm run build`     | Build your production site to `./dist/`          |
| `npm run preview`   | Preview your build locally, before deploying     |
| `npm run lint`      | Lint code with eslint                            |
| `npm run format`    | Format with Prettier                             |
| `npm run astro ...` | Run CLI commands like `astro add`, `astro check` |

## License

The following directories and their contents are Copyright Max Elkins. You may not reuse anything therein without my permission:

```
src/assets/
src/pages/project/
```

All other directories and files are MIT Licensed (where applicable).
