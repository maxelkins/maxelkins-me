// .browserlistrc influences the output of postcss-preset-env
const postcssPresetEnv = require("postcss-preset-env");
const autoprefixer = require("autoprefixer");

module.exports = {
  plugins: [
    // Place non-standard css features here
    postcssPresetEnv({
      stage: 3,
      minimumVendorImplementations: 2,
    }),
    autoprefixer(),
  ],
};
